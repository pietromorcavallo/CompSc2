diff --git a/acse/Acse.lex b/acse/Acse.lex
index 35f9b73..763e8bb 100644
--- a/acse/Acse.lex
+++ b/acse/Acse.lex
@@ -93,6 +93,7 @@ ID       [a-zA-Z_][a-zA-Z0-9_]*
 "return"          { return RETURN; }
 "read"            { return READ; }
 "write"           { return WRITE; }
+"exec"            { return EXEC; }
 
 {ID}              { yylval.svalue=strdup(yytext); return IDENTIFIER; }
 {DIGIT}+          { yylval.intval = atoi( yytext );
diff --git a/acse/Acse.y b/acse/Acse.y
index 0636dc6..4a404eb 100644
--- a/acse/Acse.y
+++ b/acse/Acse.y
@@ -90,6 +90,9 @@ t_reg_allocator *RA;       /* Register allocator. It implements the "Linear
 
 t_io_infos *file_infos;    /* input and output files used by the compiler */
 
+/* Stack containing the context information of the `exec' statements we are
+ * currently parsing. The type of each list node is `t_exec_operator*'. */
+t_list *execStack = NULL;
 
 extern int yylex(void);
 extern void yyerror(const char*);
@@ -125,6 +128,7 @@ extern void yyerror(const char*);
 %token RETURN
 %token READ
 %token WRITE
+%token EXEC
 
 %token <label> DO
 %token <while_stmt> WHILE
@@ -418,6 +422,30 @@ return_statement : RETURN
                /* insert an HALT instruction */
                gen_halt_instruction(program);
             }
+            | RETURN exp
+            {
+               /* If the execStack list is empty, we are not currently inside
+                * the block of an `exec' operator. */
+               if (execStack == NULL) {
+                  yyerror("'return' with parameter used outside an exec operator");
+                  YYERROR;
+               }
+               /* Get the context information of the innermost `exec' */
+               t_exec_operator *blockData = LDATA(execStack);
+
+               /* Generate an assignment of the expression's value to the
+                * result register. The result register contains the
+                * value of the `exec' operator. */
+               if ($2.expression_type == REGISTER) {
+                  gen_add_instruction(program, blockData->r_result, REG_0, 
+                        $2.value, CG_DIRECT_ALL);
+               } else {
+                  gen_addi_instruction(program, blockData->r_result, REG_0, 
+                        $2.value);
+               }
+               /* Generate a jump to the end of the current `exec' block. */
+               gen_bt_instruction(program, blockData->l_exit, 0);
+            }
 ;
 
 read_statement : READ LPAR IDENTIFIER RPAR 
@@ -458,7 +486,34 @@ write_statement : WRITE LPAR exp RPAR
             }
 ;
 
-exp: NUMBER      { $$ = create_expression ($1, IMMEDIATE); }
+exp: EXEC 
+   {
+      /* Push a new `exec' information structure onto the stack. */
+      t_exec_operator *blockData = malloc(sizeof(t_exec_operator));
+      execStack = addFirst(execStack, blockData);
+
+      /* Reserve a register that will contain the value of the expression.
+       * No constant folding here, it's basically impossible to do it. */
+      blockData->r_result = gen_load_immediate(program, 0);
+      /* Reserve a label that will point to the end of the block. */
+      blockData->l_exit = newLabel(program);
+   }
+   LPAR code_block RPAR
+   {
+      /* Pop the `exec' information structure from the top of the stack. */
+      t_exec_operator *blockData = LDATA(execStack);
+      execStack = removeFirst(execStack);
+
+      /* Assign the label that points to the end of the block. `return'
+       * statements jump to this label. */
+      assignLabel(program, blockData->l_exit);
+      /* Set the value of this subexpression (i.e. of this `exec' operator) */
+      $$ = create_expression(blockData->r_result, REGISTER);
+
+      /* Free the `exec' information structure as we don't need it anymore */
+      free(blockData);
+   }
+   | NUMBER      { $$ = create_expression ($1, IMMEDIATE); }
    | IDENTIFIER  {
                      int location;
    
diff --git a/acse/axe_struct.h b/acse/axe_struct.h
index 00cb86f..6d2a43e 100644
--- a/acse/axe_struct.h
+++ b/acse/axe_struct.h
@@ -110,6 +110,14 @@ typedef struct t_while_statement
                                     * that follows the while construct */
 } t_while_statement;
 
+/* `exec' operator information structure. */
+typedef struct t_exec_operator {
+   /* Label that points at the end of the block. */
+   t_axe_label *l_exit;
+   /* Register containing the result of the expression. */
+   int r_result;
+} t_exec_operator;
+
 /* create a label */
 extern t_axe_label *alloc_label(int value);
 
diff --git a/tests/exp_block/exp_block.src b/tests/exp_block/exp_block.src
new file mode 100644
index 0000000..fe00ce8
--- /dev/null
+++ b/tests/exp_block/exp_block.src
@@ -0,0 +1,13 @@
+int a, b;
+
+read(b);
+
+a = exec({
+      if (b > 10)
+        return b * 2 - 10;
+      if (b > 0)
+        return b;
+      return 0;
+    }) * 2;
+
+write(a);
