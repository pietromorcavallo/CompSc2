/******************cash-flow.mod*************/
param n;		#number of month in the future
set J:=1..n;	#set of future months

param inF{J};	#inflow of month j
param outF{J};	#outflow of month j

param initialCash;	#initial cash at the beginning of the planning period
param finalCash;	#final cash required at the end of the planning period
param optionMonths1;	#number of loan month for first option
param interestPerc1;	#interest % of the overall loan of option 1

param optionMonths2;	#number of loan month for second option
param interestPerc2;	#interest % of the overall loan of option 2

var x{J} >= 0;				#amount of money loaned with option 1 in month j
var y >= 0;					#amount of money loaned with option 2 (only at the beginning)
var cash{J}>=0;				#amount of money in cash in month j

minimize loaningCost:
	y*(1 + interestPerc2) + sum{j in J}(x[j]*(1 + interestPerc1));

subject to initializationCash:
	cash[1] = initialCash + y + x[1] + inF[1] - outF[1];

subject to balance{j in J: j > 1 and j < n}:
	cash[j] = cash[j - 1] - x[j-1]*(1 +interestPerc1) + inF[j] - outF[j] + x[j];

subject to finalMonth:
	cash[n] = cash[n - 1] -x[n - 1]*(1 +interestPerc1) + inF[n] - outF[n] + x[n] - y*(1 + interestPerc2);

subject to finalizationCash:
	cash[n] >= finalCash;

subject to cannotLoanOnLastMonth:
	x[n] = 0;
data;

param n:=8;
param inF:=
	1	100,
	2	200,
	3	200,
	4	200,
	5	150,
	6	300,
	7	500,
	8	900;
param outF:=
	1	500,
	2	500,
	3	600,
	4	600,
	5	300,
	6	200,
	7	700,
	8	100;

param initialCash:=100;
param finalCash:=50;
param optionMonths1:=8;
param interestPerc1:=0.09;

param optionMonths2:=1;
param interestPerc2:=0.015;


