# SETS

set V;
set T within V;
set E within {V,V};
set A within {V,V} :=
   E union setof{(i,j) in E} (j,i);

# PARAMS

param s symbolic in T;
param c{(i,j) in A} >= 0, default if (j,i) in E then c[j,i];
param b{h in V} = 
   (if h = s then card(T)-1
    else if h in T then -1
    else 0);

