clear
clc
close all

n_states = 3;
n_actions = 3;
gamma = 0.9;

%% MDP Solution

R_sa = [0.9*0 + 0.1*20; ...
       0.3*-2 + 0.7*-27; ...
       0.4*0 + 0.6*20; ...
       0.3*-5 + 0.7*-100; ...
       0.2*0+0.8*50];

P_sas = [0.9 0.1 0;
        0.3 0.7 0;
        0.4 0.6 0;
        0 0.3 0.7;
        0.2 0 0.8];
    
admissible_actions = [1 1 0 0 0; 0 0 1 1 0; 0 0 0 0 1];
policy = [0 1 0 0 0; 0 0 0 1 0; 0 0 0 0 1];
V = zeros(n_states,1);
V_old = ones(n_states,1);

while any(V_old ~= V)
    V_old = V;
    % Policy evaluation
    V = inv(eye(n_states) - gamma * policy * P_sas) * policy * R_sa;
    greedy_rev = R_sa + gamma * P_sas * V;
    % Policy improvement
    Q = repmat(greedy_rev',n_states,1) .* admissible_actions;
    Q(Q == 0) = - inf;
    policy = repmat(max(Q,[],2),1,5) == Q;
end
V

policy;
Q_final = [greedy_rev(1) greedy_rev(2) 0; greedy_rev(3) 0 greedy_rev(4); greedy_rev(5) 0 0]

%% RL Solution

allowed_actions = [1 1 0; 1 0 1; 1 0 0];

s = randi(3);
Q = zeros(n_states,n_actions);
M = 5000;
m = 1;

policy = @eps_greedy;

close all;
figure();

Q_hist = zeros(M,5);

while m < M
    alpha = (1 - m/M)^2;
    %eps = sqrt(1 - m/M);
    eps = (1 - m/M)^2;
    eps = 0.1;
    
    % Eps-greedy
    a = policy(s, allowed_actions, Q, eps);

    % Environment
    [s_prime, inst_rew] = transition_model(s, a);
    
    % Qlearning update
    Q(s,a) = Q(s,a) + alpha * (inst_rew + gamma * max(Q(s_prime,:)) - Q(s,a));
    s = s_prime;
    m = m + 1;

    if mod(m, 100) == 0
        clf();
        bar3(Q');
        xlabel('States');
        ylabel('Actions');
        zlim([-300 250]);
        title(['m = ' num2str(m)])
        drawnow;
    end
    
    Q_hist(m,:) = Q([1 2 3 4 8]);
end

figure()
plot(1:M, Q_hist');
legend({'Q(1,d)' 'Q(2,d)' 'Q(3,d)' 'Q(2,so)' 'Q(3,cm)'})

[Q Q_final]